import re
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from urllib.parse import urlparse


class EmailExtractSpider(CrawlSpider):
    def __init__(self, name='extractor', url=None, **kwargs):
        self.name = name
        self.allowed_domains = [urlparse(url).netloc]
        self.start_urls = [url]
        self.rules = (
            Rule(LinkExtractor(allow_domains=urlparse(url).netloc), callback='parse', follow=True),)
        super().__init__(name=name, **kwargs)

    def parse(self, response, **kwargs):
        try:
            for link in response.xpath("//a[contains(@href,'mailto:')]/@href").extract():
                email_regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')
                if re.fullmatch(email_regex, link):
                    yield {self.name: link}
        except:
            pass
