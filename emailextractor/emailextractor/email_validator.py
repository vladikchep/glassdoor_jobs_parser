import os
from email_validate.exceptions import AddressNotDeliverableError, SMTPCommunicationError, SMTPTemporaryError
from email_validate import validate, validate_or_fail
import re

for filename in os.listdir("/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/filtered"):
    if filename.endswith(".txt"):
        file_name = f'/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/filtered/{os.path.splitext(os.path.join(filename))[0]}.txt'
        file_name_output = f"/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/validated/{os.path.splitext(os.path.join(filename))[0]}.txt"
        with open(f"{file_name}", "r") as file1:
            uniqlines = file1.readlines()
        with open(f"{file_name_output}", "w") as file:

            for line in uniqlines:
                match = re.search(r'[\w\\.-]+@[\w\\.-]+', line)
                try:
                    if validate_or_fail(email_address=f'{str(match.group(0)).strip()}', check_format=True,
                                        check_blacklist=True,
                                        check_dns=True, dns_timeout=10, check_smtp=False, smtp_debug=False):
                        file.write('Verified\t' + line)

                except (AddressNotDeliverableError, SMTPCommunicationError, SMTPTemporaryError):
                    file.writelines('Uncertain\t' + line)
                except:
                    file.write('NOT Verified\t' + line)
