import os
from sortedcontainers import SortedSet

for filename in os.listdir("/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/emails"):
    if filename.endswith(".txt"):
        file_name = f'/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/emails/{os.path.splitext(os.path.join(filename))[0]}.txt'
        file_name_output = f"/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/filtered/{os.path.splitext(os.path.join(filename))[0]}.txt"
        try:
            uniqlines = SortedSet(open(file_name, 'r', encoding='utf-8').readlines())
            gotovo = open(file_name_output, 'w', encoding='utf-8').writelines(SortedSet(uniqlines))
        except:
            print(os.path.splitext(os.path.join(filename))[0])
