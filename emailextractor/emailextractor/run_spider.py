from scrapy.utils.log import configure_logging
from twisted.internet import reactor, defer
import pandas as pd
from scrapy.crawler import CrawlerRunner

from emailextractor.emailextractor.spiders.email_spider import EmailExtractSpider

article_read = pd.read_csv(
    f'/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/glassdoor_companies_lists/Java_United Kingdom_list.csv',
    delimiter=',')
conten_lst = []
for i in range(article_read.shape[0]):
    if pd.isna(article_read.iloc[i, 3]) is True:
        continue
    else:
        conten_lst.append([article_read.iloc[i, 1], article_read.iloc[i, 3]])

custom_settings = {
    "FEED_URI": f"/home/developer/Documents/glassdoor_jobs_parser/emailextractor/emailextractor/emails/Java_United Kingdom_list_company_email.txt",
    "DEPTH_LIMIT": 1,
    'CLOSESPIDER_TIMEOUT': 15
}
configure_logging()
runner = CrawlerRunner(custom_settings)


@defer.inlineCallbacks
def crawl(sites):
    for item_url in sites:
        yield runner.crawl(EmailExtractSpider, name=item_url[0], url=item_url[1])
    reactor.stop()


crawl(conten_lst)
reactor.run()
