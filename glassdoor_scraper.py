from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import pandas as pd
from selenium.webdriver.chrome.service import Service
from pathlib import Path


def delete_duplicates(jobs):
    return list({v['Company Name']: v for v in jobs}.values())


def get_jobs(keyword, location):
    jobs_in_page = 31
    slp_time = 2
    job_title, company_name, job_description, url = None, None, None, None

    options = webdriver.ChromeOptions()
    prefs = {"profile.managed_default_content_settings.images": 2}
    options.add_experimental_option("prefs", prefs)
    options.add_argument("--headless")
    path = Path(__file__).parent.absolute().joinpath('chromedriver.exe')
    serv = Service(str(path))
    driver = webdriver.Chrome(service=serv, options=options)
    # driver.set_window_size(1120, 1000)
    driver.get(
        'https://www.glassdoor.com/Job/jobs.htm?suggestCount=0&suggestChosen=false&clickSource=searchBtn&\
        typedKeyword=&sc.keyword=&locT=&locId=&jobType=')
    search = driver.find_element(By.ID, "KeywordSearch")
    search.send_keys(keyword)
    driver.find_element(By.ID, "LocationSearch").clear()
    search = driver.find_element(By.ID, "LocationSearch")
    search.send_keys(location)
    search.send_keys(Keys.RETURN)
    driver.implicitly_wait(slp_time)
    location = None
    try:
        driver.find_element(By.XPATH, '//*[@id="MainCol"]/div[1]/ul/li[1]').click()
    except ElementClickInterceptedException:
        pass
    driver.implicitly_wait(1)
    try:
        driver.find_element(By.CSS_SELECTOR, '[alt="Close"]').click()
    except NoSuchElementException:
        pass
    jobs = []
    page_counter = 0
    while True:

        for item in range(1, jobs_in_page):
            driver.implicitly_wait(1)

            try:
                driver.find_element(By.XPATH, f'//*[@id="MainCol"]/div[1]/ul/li[{item}]').click()
            except NoSuchElementException:
                break
            except:
                driver.implicitly_wait(slp_time)
                # continue
            try:
                driver.implicitly_wait(slp_time)
                # company site
                url = driver.find_element(By.XPATH, '//*[@id="EmpBasicInfo"]/div[2]/div/a').get_attribute('href')
            except NoSuchElementException:
                url = None
            except:
                driver.implicitly_wait(slp_time)

            try:
                # job title
                job_title = driver.find_element(By.XPATH, f'//*[@id="MainCol"]/div[1]/ul/li[{item}]/div[2]/a/span').text
                # Location
                location = driver.find_element(By.XPATH,
                                               f'//*[@id="MainCol"]/div[1]/ul/li[{item}]/div[2]/div[2]/span').text
                # company name
                company_name = driver.find_element(By.XPATH,
                                                   f'//*[@id="MainCol"]/div[1]/ul/li[{item}]/div[2]/div[1]/a/span').text
            except:
                driver.implicitly_wait(slp_time)

            if None not in [job_title, company_name]:
                jobs.append({"Job Title": job_title,
                             "Company Name": company_name,
                             "Location": location,
                             "Web site": url,
                             })
        page_counter+=1
        try:
            if driver.find_element(By.CLASS_NAME, 'e1gri00l2').get_attribute('disabled') or page_counter == 30:
                return pd.DataFrame(delete_duplicates(jobs))
            driver.find_element(By.CLASS_NAME, 'e1gri00l2').click()
        except NoSuchElementException:
            break
        except ElementClickInterceptedException:
            break
        except:
            break
    return pd.DataFrame(delete_duplicates(jobs))


locations = ["Australia", "Norway", "Sweden", "Germany", "Finland", "Spain", "France", "Italy", "Denmark", "Poland",
             "Austria", "Czech Republic", "United Kingdom", "United Arab Emirates","United States","United Kingdom"]


key_words = ["Java", "JavaScript", "PHP"]

for location in locations:
    for key_word in key_words:
        df = get_jobs(key_word, location)
        df.to_csv(f'{key_word}_{location}_list.csv', index=False)
